# Documentación del Proyecto: Sr DevOps Challenge

Este documento presenta la documentación completa del desafío técnico Sr DevOps, detallando el enfoque, las tecnologías utilizadas, y las prácticas implementadas para lograr una solución eficiente y escalable.

---

## Tabla de Contenidos
1. [Descripción General del Proyecto y Introducción](#descripción-general-del-proyecto-y-introducción)
2. [Estructura y Organización](#estructura-y-organización)
    - [Arquitectura del Proyecto](#arquitectura-del-proyecto)
    - [Organización de Directorios](#organización-de-directorios)
3. [Automatización y CI/CD](#automatización-y-cicd)
    - [Pipeline de Infraestructura](#pipeline-de-infraestructura)
    - [Pipeline de Aplicación](#pipeline-de-aplicación)
4. [Elección de Herramientas y Gestión de Infraestructura](#elección-de-herramientas-y-gestión-de-infraestructura)
    - [Herramientas y Tecnologías Utilizadas](#herramientas-y-tecnologías-utilizadas)
    - [Gestión de Infraestructura como Código](#gestión-de-infraestructura-como-código)
5. [Guía de Uso](#guía-de-uso)
    - [Cómo Implementar Cambios](#cómo-implementar-cambios)
    - [Uso del Sistema en Diferentes Entornos](#uso-del-sistema-en-diferentes-entornos)
6. [Conclusiones y Reflexiones](#conclusiones-y-reflexiones)
    - [Aprendizajes Clave](#aprendizajes-clave)
    - [Reflexiones Finales](#reflexiones-finales)

---
## Descripción General del Proyecto y Introducción

Este proyecto se ha desarrollado como parte de un desafío técnico para una posición de Sr DevOps. El objetivo principal es demostrar habilidades avanzadas en la implementación y gestión de infraestructura y aplicaciones utilizando tecnologías de vanguardia en el campo de DevOps.

El alcance del proyecto abarca la creación de un entorno multi-escenario utilizando herramientas modernas de automatización e infraestructura como código (IaC). La solución propuesta implica el despliegue de una aplicación de demostración en un clúster de Kubernetes, gestionado a través de Terraform, Helmfile y Helm secrets, dentro de un entorno de integración y entrega continua (CI/CD). Se utilizó la imagen `nginxdemos/hello` desde Docker Hub como base para la aplicación.

El reto específico incluye:
- Implementar un despliegue multi-escenario con Helmfile.
- Asegurar una configuración diferenciada para los entornos de 'desarrollo' (dev) y 'staging' (stage).
- Modificar la imagen de Docker para incluir detalles específicos del entorno y valores secretos.
- Usar Terraform para crear la infraestructura necesaria en la nube, específicamente en Azure Kubernetes Service (AKS).
- Integrar un sistema de CI/CD que facilite el despliegue automático en respuesta a cambios en el código.

Los desafíos técnicos incluyeron el dominio de nuevas tecnologías y plataformas, particularmente en lo que respecta a Azure y Terraform, así como la integración y gestión eficiente de secretos y configuraciones para diferentes entornos.

El resultado esperado del proyecto es una demostración efectiva de habilidades en la implementación de una infraestructura robusta y escalable, así como en la automatización del flujo de trabajo de DevOps, reflejando un entendimiento profundo de las mejores prácticas en la industria.

---

## Estructura y Organización

### Arquitectura del Proyecto
La arquitectura del proyecto se diseñó meticulosamente para reflejar las mejores prácticas en DevOps y gestión de infraestructura. Se centró en la creación de un entorno Kubernetes en Azure (AKS), utilizando Terraform para la infraestructura como código (IaC) y Helmfile junto con Helm secrets para la gestión eficiente de las aplicaciones y sus configuraciones.

La infraestructura de Kubernetes en Azure se eligió por su escalabilidad, fiabilidad y la amplia gama de características que ofrece para la orquestación de contenedores. Terraform se utilizó para implementar esta infraestructura de manera reproducible y modular, permitiendo una gestión y actualización eficientes.

### Organización de Directorios
El proyecto se organizó en dos segmentos principales: `Infrastructure` y `Products`.

#### `Infrastructure`
Bajo esta sección, se utilizó Terraform para definir y gestionar la infraestructura necesaria. La estructura se dividió en `Implementations` y `Modules`. 
- `Implementations`: Contiene la configuración específica para desplegar la infraestructura de AKS en Azure.
- `Modules`: Alberga módulos reutilizables de Terraform, facilitando la escalabilidad y mantenimiento del código.

#### `Products/N5-challenge/demo-app`
Esta sección aloja la aplicación de demostración y todo lo necesario para su despliegue y gestión.
- `Dockerfile`: Para construir la imagen personalizada de la aplicación.
- `infrastructure`: Incluye configuraciones de Helm y Helmfile para gestionar diferentes entornos (dev y stage) y la definición del chart de Helm para la aplicación.
- `sources`: Contiene el código fuente de la aplicación, en este caso, un `index.html` simple.

La estructura de directorios fue diseñada para maximizar la eficiencia, la claridad y la facilidad de mantenimiento, asegurando que cada componente del proyecto sea fácilmente accesible y modificable.

---

## Automatización y CI/CD

La automatización y la integración y entrega continua (CI/CD) son fundamentales en este proyecto, garantizando un flujo de trabajo eficiente y confiable. Se implementaron dos pipelines principales de CI/CD: uno para la infraestructura y otro para la aplicación.

### Pipeline de Infraestructura
El pipeline de infraestructura está diseñado para gestionar y actualizar automáticamente la infraestructura en Azure Kubernetes Service (AKS). Utilizando Terraform, este pipeline automatiza los siguientes pasos:
- **Inicialización y Planificación**: Prepara el entorno de Terraform y muestra los cambios planeados.
- **Aplicación**: Aplica los cambios definidos en Terraform para modificar la infraestructura.
- **Exportación de Configuraciones**: Al finalizar, exporta configuraciones clave como artefactos, incluyendo el `kubeconfig` y las credenciales necesarias para la gestión del clúster.

Este pipeline asegura que cualquier cambio en la infraestructura sea gestionado de forma controlada y documentada, reduciendo significativamente el riesgo de errores humanos.

### Pipeline de Aplicación
El pipeline de la aplicación se encarga de la construcción, prueba y despliegue de la aplicación `nginxdemos/hello`. Los pasos clave incluyen:
- **Construcción de la Imagen**: A partir del `Dockerfile`, construye una imagen de Docker y la almacena en un registro de contenedores.
- **Pruebas Automatizadas**: Ejecuta pruebas para validar la integridad y el correcto funcionamiento de la imagen.
- **Despliegue con Helmfile**: Utiliza Helmfile para desplegar la imagen en el clúster de Kubernetes, asegurando que la configuración sea apropiada para el entorno seleccionado (dev o stage).

Este pipeline facilita el proceso de llevar los cambios desde el desarrollo hasta la producción de manera rápida y segura, garantizando que la aplicación esté siempre actualizada y funcionando correctamente.

---

## Elección de Herramientas y Gestión de Infraestructura

La selección de herramientas y la gestión de la infraestructura son cruciales para el éxito del proyecto. Este proyecto integró diversas herramientas, cada una elegida por su eficacia, capacidad de integración y amplio uso en la industria de DevOps.

### Herramientas y Tecnologías Utilizadas
- **Terraform**: Elegido por su capacidad para implementar infraestructura como código de manera eficiente y su compatibilidad con múltiples proveedores de nube, incluido Azure.
- **Kubernetes (AKS)**: Utilizado para la orquestación de contenedores debido a su robustez, escalabilidad y amplia adopción en la industria.
- **Helmfile y Helm Secrets**: Estas herramientas facilitan la gestión de aplicaciones en Kubernetes, permitiendo una configuración detallada y segura de las aplicaciones.
- **Docker**: Esencial para la creación de contenedores, permitiendo una portabilidad y consistencia en el despliegue de aplicaciones.

### Gestión de Infraestructura como Código
La infraestructura como código (IaC) es una práctica central en este proyecto. Terraform se utilizó para codificar la infraestructura necesaria en Azure, lo que permitió una implementación y gestión sistemáticas y reproducibles de los recursos de la nube. La modularidad y la reusabilidad de Terraform facilitaron la escalabilidad y el mantenimiento del código de infraestructura.

Este enfoque garantiza que todos los cambios en la infraestructura sean versionados, documentados y trazables, lo que resulta en una gestión de infraestructura más segura y eficiente. Además, la IaC fomenta una cultura de DevOps más colaborativa y transparente, ya que todo el equipo puede revisar y contribuir al código de infraestructura.

---

## Guía de Uso

Esta sección ofrece una guía detallada sobre el uso de los pipelines de CI/CD configurados en el proyecto, y cómo gestionar los cambios en la aplicación y sus parámetros.

### Uso de los Pipelines de CI/CD
Los pipelines de CI/CD están diseñados para automatizar completamente el proceso de despliegue, tanto de la infraestructura como de la aplicación.

1. **Para la Aplicación**:
   - Los cambios en la aplicación deben realizarse principalmente dentro de la carpeta `sources`.
   - Al actualizar el contenido de `sources` y hacer commit y push al repositorio, el pipeline de CI/CD de la aplicación se activará automáticamente. Este proceso incluye la reconstrucción de la imagen Docker (si es necesario) y su despliegue en el clúster de Kubernetes correspondiente.

2. **Para la Infraestructura**:
   - Cualquier cambio en la configuración de Terraform debe seguir el mismo proceso de commit y push, activando así el pipeline de CI/CD de infraestructura para aplicar los cambios en Azure.

### Gestión de Configuraciones y Secretos
El manejo de configuraciones y secretos se realiza a través de los archivos `values.yaml` y `secrets.enc.yaml` dentro de la carpeta `infrastructure/environments/[ENV]`. (ENV: dev, stage)

- **Values**: Los archivos `values.yaml` en los directorios de entorno contienen parámetros configurables para la aplicación y el entorno. Estos pueden ser modificados según sea necesario para reflejar los cambios en la configuración.

- **Secrets**: Para los secretos, se utiliza `helm secrets`, que depende de Mozilla SOPS para la encriptación. Los pasos para manejar secretos son los siguientes:
    - Asegúrate de que la CLI de Azure esté instalada y autenticada en tu sistema.
    - Configura el archivo `.sops.yaml` con la URL del Key Vault de Azure, donde se almacena la clave de encriptación.
    - Los secretos deben ser agregados o modificados en los archivos `secrets.enc.yaml` y luego encriptados utilizando SOPS.
    - Al hacer commit y push de los cambios, los secretos encriptados se desplegarán de forma segura mediante el pipeline de CI/CD.

Siguiendo estas pautas, se garantiza que los cambios en la aplicación y su configuración se manejen de manera segura y eficiente, con una mínima intervención manual y máxima automatización a través de los pipelines de CI/CD.

---

## Conclusiones y Reflexiones

El desafío técnico Sr DevOps presentó una oportunidad excepcional para demostrar habilidades y competencias en el ámbito de DevOps, aunque también destacó aspectos que, aunque no eran requisitos del desafío, son cruciales en proyectos de mayor envergadura y seguridad.

### Reflexiones sobre el Proyecto
1. **Mea Culpa en Seguridad y Gestión Avanzada**: Si bien el proyecto cubrió los requerimientos del desafío, hay áreas de mejora y consideraciones que podrían haberse implementado en un escenario de producción real:
   - **Seguridad Mejorada**: Incluir seguridad de repositorios, análisis de seguridad tanto a nivel de infraestructura como de aplicación.
   - **Redes y Acceso Controlado**: Configurar una red privada para la infraestructura, con acceso controlado a Internet solo para aplicaciones que lo requieran, y el resto operando a través de una VPN.
   - **Certificados y Credenciales**: Implementar un sistema más robusto para la gestión de certificados y credenciales.
   - **Runners de GitLab Privados**: Crear runners de GitLab dentro de la infraestructura de Azure para mayor control y seguridad.

2. **Reafirmación de Competencias y Diversión en los Desafíos**: A pesar de las áreas no exploradas, el proyecto fue una experiencia divertida que permitió aplicar y demostrar un alto nivel de habilidad en DevOps y la automatización de infraestructuras.

### Reflexiones Finales
Este desafío sirvió para reafirmar la capacidad de liderar y ejecutar proyectos complejos de DevOps. Aunque se centró en las demandas específicas del desafío, destacó la importancia de considerar aspectos adicionales de seguridad y gestión de infraestructura en un entorno de producción real.

