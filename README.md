# Aplicación Demo

## Descripción
Este repositorio contiene la aplicación de demostración utilizada en el proyecto Sr DevOps Challenge. Incluye el código fuente, Dockerfile, y configuraciones de Helm necesarias para el despliegue en un entorno de Kubernetes.

## Estructura del Repositorio
- `Dockerfile`: Define la imagen de Docker para la aplicación.
- `sources/`: Contiene el código fuente de la aplicación (ej. `index.html`).
- `infrastructure/`: Incluye las configuraciones de Helm y Helmfile.
  - `environments/`: Almacena los archivos de configuración para los distintos entornos (dev y stage).
  - `helmchart/`: Contiene el chart de Helm para desplegar la aplicación.

## Uso
Para realizar cambios en la aplicación, actualice los archivos en la carpeta `sources` y haga commit y push al repositorio. El pipeline de CI/CD se encargará del despliegue automático en el clúster de Kubernetes correspondiente.

## Configuración de Entornos
Modifique los archivos en `infrastructure/environments/` para ajustar configuraciones específicas de cada entorno. Para secretos, utilice `helm secrets` para una gestión segura.
